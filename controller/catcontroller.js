var Model = require('../models');
module.exports.getAll = (req, res) => {
    Model.Groups.findAll({
        include: {

            model: Model.Categories,
            include:{
                model:Model.Products
            },

            // where: { isactive: 'true' },

            // attributes: ['name'],
        },
       
          })
        .then((user) => {

            res.send(user);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Some error occurred while retrieving tutorials."

            });

        });

};
//getting data form groups how is active 
module.exports.getisActive = (req, res) => {
    Model.Groups.findAll({where:{isactive:"true"}
          })
        .then((user) => {

            res.send(user);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Some error occurred while retrieving tutorials."

            });

        });

};