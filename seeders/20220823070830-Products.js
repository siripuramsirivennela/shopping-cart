'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     * 
     *   name: string , description: string, image_url: string, categoryId: integer foreign key(association to Categories table) , isactive: boolean
    */
     await queryInterface.bulkInsert('Products', [{
      name: 'store3',
      description: 'p3',
       CategoryId: '2',
       image_url:'imgurl3',

       isactive: 'true' ,
       createdAt:new Date(),
       updatedAt:new Date()

  },])
},

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Products', null, {});
  }
};
